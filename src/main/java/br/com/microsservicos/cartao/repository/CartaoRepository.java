package br.com.microsservicos.cartao.repository;

import br.com.microsservicos.cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findById(int id);

    Optional<Cartao> findByNumero(int numero);
}
