package br.com.microsservicos.cartao.controller;

import br.com.microsservicos.cartao.DTOs.CartaoDTO;
import br.com.microsservicos.cartao.clients.Cliente;
import br.com.microsservicos.cartao.clients.ClienteClient;
import br.com.microsservicos.cartao.models.Cartao;
import br.com.microsservicos.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController//transforma em classe controladora do spring
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteClient clienteClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody @Valid Cartao cartao){
        Cartao objetoCartao = cartaoService.salvarCartao(cartao);
        return objetoCartao;
    }

    @GetMapping("/{id}")
    public CartaoDTO buscarCartaoPeloId(@PathVariable(name = "id") int id){
        try{
            Cartao cartao = cartaoService.buscarCartaoPeloId(id);
            Cliente byIdCliente = clienteClient.getById(cartao.getClienteId());

            return cartaoService.transformarEmDTO(cartao, byIdCliente.getName());

        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{numero}")
    public Cartao ativaCartao(@RequestBody @PathVariable(name="numero") int numero){
        try {
            Cartao cartaoDB= cartaoService.ativaCartao(numero);
            return cartaoDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
