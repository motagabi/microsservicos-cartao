package br.com.microsservicos.cartao.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
    @GetMapping("cliente/{id}")
    Cliente getById(@PathVariable int id);
}
