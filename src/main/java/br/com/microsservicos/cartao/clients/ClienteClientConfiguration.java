package br.com.microsservicos.cartao.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {
    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new ClienteClientDecoder();
    }

}

