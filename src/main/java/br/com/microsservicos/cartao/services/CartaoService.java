package br.com.microsservicos.cartao.services;
import br.com.microsservicos.cartao.DTOs.CartaoDTO;
import br.com.microsservicos.cartao.clients.Cliente;
import br.com.microsservicos.cartao.clients.ClienteClient;
import br.com.microsservicos.cartao.models.Cartao;
import br.com.microsservicos.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao salvarCartao(Cartao cartao) {
        Cliente cliente = clienteClient.getById(cartao.getClienteId());
        if ( cliente != null ) {
            try {
                cartao.setClienteId(cliente.getId());
                cartao.setAtivo(false);//criar sempre como inativo
                return cartaoRepository.save(cartao);

            } catch (RuntimeException ex) {
                throw new RuntimeException("Erro ao salvar cartão: " + ex.getMessage());
            }
        } else {
            throw new RuntimeException("Cliente id: (" + cartao.getClienteId() + ") inválido ou inexistente.");
        }


    }
    public Cartao buscarCartaoPeloId(int id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }else{
            throw new RuntimeException("O cartão "+ id +" não foi encontrado");
        }
    }

    public Cartao buscarCartaoPeloNumero(int numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if(cartaoOptional.isPresent()){
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }else{
            throw new RuntimeException("O cartão "+ numero +" não foi encontrado");
        }
    }
    public Cartao ativaCartao(int numero){
        Cartao cartaoDB = buscarCartaoPeloNumero(numero);
        if(cartaoDB.isAtivo()){
            cartaoDB.setAtivo(false);
        }else{
            cartaoDB.setAtivo(true);
        }
        cartaoDB.setId(cartaoDB.getId());
        return cartaoRepository.save(cartaoDB);
    }
    public CartaoDTO transformarEmDTO(Cartao cartao, String nome) {
        CartaoDTO cartaoDTO = new CartaoDTO();
        cartaoDTO.setClienteId(cartao.getClienteId());
        cartaoDTO.setClienteNome(nome);
        cartaoDTO.setId(cartao.getId());
        cartaoDTO.setNumeroCartao(cartao.getNumero());
        cartaoDTO.setAtivo(cartao.isAtivo());
        return cartaoDTO;
    }
}
