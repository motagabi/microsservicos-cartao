

Introdução
Uma startup está com planos de lançar seu  sistema de cartões, para isso, ela entrou em  contato com você para desenvolver um sistema MVP que permita:

Cadastrar um novo cliente
Criar um novo cartão
Ativar um novo cartão
Fazer uma compra em um cartão
Consultar a fatura de um cartão

Obs: Por ser apenas um MVP, esse sistema não irá  a público então não há necessidade de um  sistema  de autenticação

Regras de negocio:

Não deve ser possivel fazer uma compra em um cartão que não existe
Não deve ser possivel criar um cartão de um cliente que não exite
Não deve ser possivel criar um pagamento de um cartão inativo
